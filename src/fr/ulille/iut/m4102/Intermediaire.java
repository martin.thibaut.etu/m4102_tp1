package fr.ulille.iut.m4102;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice et
 * l'implémentation du traitement des chaînes. Au début cette classe se contente
 * de logger ce qui se passe puis elle va évoluer pour accéder au service à
 * distance. Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */
public class Intermediaire implements AlaChaineInterface {
	// private AlaChaine alc;
	// private AccesService as;

	public Intermediaire() {
		// pour l'instant on accède directement au service après instanciation
		// alc = new AlaChaine();
		// as = new AccesService();

	}

	public int nombreMots(String chaine) {
		Client client = new Client("localhost", 8080);
		System.out.println("Appel de : nombreMots(String " + chaine + ")");
		System.out.println("Return : int = return le nombre de mots de la chaine");
		return Integer.parseInt(client.envoyer("CALL:nombreMots:param[String,\"" + chaine + "\"]"));
	}

	public String asphyxie(String chaine) throws PasDAirException {
		Client client = new Client("localhost", 8080);
		System.out.println("----------------------------------");
		System.out.println("Appel de : asphyxie(String " + chaine + ")");
		System.out.println("Return : String = return la chaine sans les r ou R si il y en a un sinon un exeption");
		System.out.println("Exeption : PasDAirException = affiche le message qui ai passé en parametres de l'exeption");
		return client.envoyer("CALL:asphyxie:param[String,\"" + chaine + "\"]");
	}

	public String leetSpeak(String chaine) {
		Client client = new Client("localhost", 8080);
		System.out.println("----------------------------------");
		System.out.println("Appel de : leetSpeak(String " + chaine + ")");
		System.out.println(
				"Return : String = return la chaine en changeant tout les Aa par 4, Ee par 3, Ii par 1 et Oo par 0");
		return client.envoyer("CALL:leetSpeak:param[String,\"" + chaine + "\"]");
	}

	public int compteChar(String chaine, char c) {
		Client client = new Client("localhost", 8080);
		System.out.println("----------------------------------");
		System.out.println("Appel de : compteChar(String " + chaine + ", char " + c + ")");
		System.out.println("Return : int = return le nombre de char passé en parametres");
		return Integer
				.parseInt(client.envoyer("CALL:compteChar:param[String,\"" + chaine + "\"]:param[char,\'" + c + "\']"));
	}

}
