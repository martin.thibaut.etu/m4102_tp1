package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Cette classe se trouvera côté service Elle recevra une requête sous forme de
 * chaîne de caractère conforme à votre protocole. Elle transformera cette
 * requête en une invocation de méthode sur le service puis renverra le résultat
 * sous forme de chaîne de caractères.
 */
public class AccesService implements Runnable {
	private AlaChaine alc;
	private Socket client;

	public AccesService() {
		alc = new AlaChaine();
	}

	public AccesService(Socket s) {
		alc = new AlaChaine();
		client = s;
	}

	public String traiteInvocation(String invocation) {
		// ici, il faudra décomposer la chaîne pour retrouver la méthode appelée
		// et les paramètres, réaliser l'invocation sur la classe AlaChaine puis
		// renvoyer le résultat sous forme de chaîne.
		String result = "";

		String[] invoc = invocation.split(":");
		if (invoc.length != 1) {
			for (int i = 0; i < invoc.length; i++) {
				if (invoc[1].equals("nombreMots")) {
					result = "" + alc.nombreMots(invoc[2].split("\"")[1]);
				} else if (invoc[1].equals("asphyxie")) {
					try {
						result = alc.asphyxie(invoc[2].split("\"")[1]);
					} catch (PasDAirException e) {
						result = e.getMessage();
					}
				} else if (invoc[1].equals("leetSpeak")) {
					result = alc.leetSpeak(invoc[2].split("\"")[1]);
				} else if (invoc[1].equals("compteChar")) {
					result = "" + alc.compteChar(invoc[2].split("\"")[1], invoc[3].split("\'")[1].charAt(0));
				}
			}
		}
		return result;
	}

	public void traiteRequete() {
		try {
			BufferedReader reception = new BufferedReader(new InputStreamReader(client.getInputStream()));
			PrintWriter envoi = new PrintWriter(client.getOutputStream(), true);
			//System.out.println(reception.readLine());
			envoi.println(this.traiteInvocation(reception.readLine()));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		traiteRequete();
	}
}
